#!/usr/bin/env python
# coding: utf-8

# In[1]:


class Cuadrado:
    def __init__(self,lado):
        self.__lado = lado
        
    def calculo_superficie(self):
        self.__area = self.__lado ** 2
        return self.__area
    
    def SetLado(self,lado):
        self.__lado=lado
        self.__area=self.calculo_superficie() ## Al mismo tiempo que se use el Setter para lado, se calcula el área. 
        
    def GetLado(self):
        return self.__lado
    
    def GetArea(self):
        if self.__area is None:
            self.__area = self.calculo_superficie()
        return self.__area

import math
class Circulo:
    def __init__(self,radio,coordenadas):
        self.radio=radio
        self.coordenadas = coordenadas

    def calculoSuperficie(self):
        self.area=math.pi*self.radio**2
        return self.area

    def SetRadio(self,radio):
        self.radio=radio
    def GetRadio(self):
        return self.radio
    
    def mostrarDatos(self):
        print("Radio =",str(self.radio),"\nCoordenadas("+str(self.coordenadas.x)+","+str(self.coordenadas.y)+")") 

class Triangulo:
    def __init__(self,base,altura):
        self.base = base
        self.altura = altura

    def calculoSuperficie(self):
        self.area = (self.base*self.altura)/2
        return self.area

    def SetBase(self,base):
        self.base = base

    def SetAltura(self,altura):
        self.altura = altura

class Coordenadas:
    def __init__(self,x,y):
        self.x = x
        self.y = y

        
"""Para practicar, pueden imprimir también el área de cada una de las figuras, se tendría que agregar
   dicho atributo en las funciones mostrarDatos()"""
##CIRCULO##
coordenadasCirculo = Coordenadas(4,5) 
radio=int(input('Introduce el radio:')) 
print(type(coordenadasCirculo))
print(type(5))
print(type("string"))
circuloEjemplo = Circulo(radio,Coordenadas(4,5))
circuloEjemplo.mostrarDatos()
#########################

##CUADRADO##

########################

##TRIÁNGULO##

########################


# In[ ]:




